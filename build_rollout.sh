#!/bin/sh

#git checkout master
#git pull origin master

## Global Variables
prefix="3M-DHL"
mocadir='pkg\src\cmdsrc\usrint\'
mocasrcdir='$LESDIR\src\cmdsrc\usrint\'
csvdir='pkg\db\data\load\base\bootstraponly\'
tocsvdir='$LESDIR\db\data\load\base\bootstraponly\'
unloaddir='pkg\db\data\load\base\bootstraponly\sl_unload\'
scriptdir='pkg\db\scripts\'
reportdir='pkg\reports\usrint\'
toreportdir='$LESDIR\reports\usrint\'
mbuild=0
seperator='\'
loadloop=0

##define functions
createfolder()
{
    seperator='\'
    todir="$1$seperator$2"
    
    ##set to lowercase
    todir="${todir,,}" 
    
    ##create directory if it does not exist
    if [[ ! -d "$todir" ]]
        then
            install -d "$todir"                       
    fi
}

##lets create the the rollout folder
dirname="$prefix-$1"

if [[ ! -d "$dirname" ]]
    then
        #create
        mkdir ./$dirname
     
    else
        #delete and create again
        rm -rf "$dirname"
        mkdir ./$dirname
fi

#Lets start building the file
echo '######################################################' >> "$dirname"'\'"$dirname.txt"
echo '### Rollout Creation Script:    Vivek Mohan   2018 ###' >> "$dirname"'\'"$dirname.txt"
echo '######################################################' >> "$dirname"'\'"$dirname.txt"
echo "" >> "$dirname"'\'"$dirname.txt"

##copy rollout.pl
cp "rollout.pl" "$dirname"

#git log -S $1 --pretty=format:"%H"
##Get Commits for inputted string $1

#git log --pretty=format:"%H" --all --grep "$1"
commitarr=(`git log --pretty=format:"%H" --all --grep "$1"`)
 
#echo ${filearr[0]}

for i in "${commitarr[@]}"
do
   #get the filenames into an array
   filelistarr+=(`git show $i --pretty="" --no-commit-id --name-only`)  
   
   
done

#get only uniqye values
eval filearr=($(printf "%q\n" "${filelistarr[@]}" | sort -u))

#loop through files and check it exists
##within loop
###determine file type and folder
###copy file to new folder


#First loop for replace
echo "##COPYING FILES" >> "$dirname"'\'"$dirname.txt"   
for i in "${filearr[@]}"
do
    echo "Adding $i"
  if [ -f $i ]
        then            
            case "$i" in 
                *.mtrg) 
                
                    #create folder
                    createfolder $dirname $mocadir 
                    
                    #copy file
                    cp $i $todir   
                    
                    #setmbuild
                    mbuild=1
                    
                    #we need to get the filename without the path
                    justfilename=${i##*/}
                    
                    tr A-Z a-z < $justfilename
                    echo "$justfilename"
                    
                    ##Lets sort the file out, for moca we simply need a replace                    
                    echo "REPLACE $mocadir$justfilename $mocasrcdir" >> "$dirname"'\'"$dirname.txt"                    
                    
                      
                ;;                   
                *.mcmd) 
                
                    #create folder
                    createfolder $dirname $mocadir 
                    
                    ##copy file
                    cp $i $todir        
                    
                    #setmbuild
                    mbuild=1
                    
                    #we need to get the filename without the path
                    justfilename=${i##*/}
                    
                    ##Lets sort the file out, for moca we simply need a replace                    
                    echo "REPLACE $mocadir$justfilename $mocasrcdir" >> "$dirname"'\'"$dirname.txt"                   
                    
                ;;
                 *.jrxml) 
                
                    #create folder
                    createfolder $dirname $reportdir 
                    
                    ##copy file
                    cp $i $todir                                                                   
                    
                    #we need to get the filename without the path
                    justfilename=${i##*/}
                    
                    ##Lets sort the file out, for moca we simply need a replace                    
                    echo "REPLACE $reportdir$justfilename $toreportdir" >> "$dirname"'\'"$dirname.txt"                   
                    
                ;;
                *.csv)
                    #create folder
                    ##we need to get filename and create folder for CSV
                    file=${i##*/}
                    file="${file,,}"
                    filepart=${file%.*}                      
                                        
                    newcsvdir="$csvdir$filepart"                     
                                        
                    createfolder $dirname $newcsvdir         
                    
                    ##copy file
                    cp $i "$todir$seperator$file"      
                    
                    ##We need to now add into the instruction file to move this to the relevant folder              
                    
                    echo "REPLACE $newcsvdir$seperator$file $tocsvdir$filepart$seperator" >> "$dirname"'\'"$dirname.txt"      
                    
                
                ;;
                *.ctl)
                    #create folder
                    ##we need to get filename and create folder for CSV
                    file=${i##*/}
                    file="${file,,}"
                    filepart=${file%.*}      
                                   
                    newcsvdir="$csvdir"
                                        
                    createfolder $dirname $newcsvdir         
                    
                    ##copy file
                    cp $i $todir
                                        
                    ##We need to now add into the instruction file to move this to the relevant folder which is one up from the poldat                    
                                     
                    echo "REPLACE $csvdir$file $tocsvdir" >> "$dirname"'\'"$dirname.txt"
                    
                    #we want to run the load file loop
                    loadloop=1
                                                                         
                
                ;;
                *.msql)                                                                    
                    #Will load from package so just need to add into package
                    #create folder
                    createfolder $dirname $scriptdir 
                    
                    #copy file
                    cp $i $todir   
                    justfilename=${i##*/}
                    
                    echo "# Skipping $justfilename as will load within package" >> "$dirname"'\'"$dirname.txt"  
                    
                    #we want to run the load file loop
                    loadloop=1
                ;;
                *.sql)
                    #Will load from package so just need to add into package
                    #create folder
                     createfolder $dirname $scriptdir 
                    
                    #copy file
                    cp $i $todir
                    
                    justfilename=${i##*/}
                    echo "# Skipping $justfilename as will load within package" >> "$dirname"'\'"$dirname.txt"  
                    
                    #we want to run the load file loop
                    loadloop=1
                ;;
                *.seq)
                    #Will load from package so just need to add into package
                    #create folder
                     createfolder $dirname $scriptdir 
                    
                    #copy file
                    cp $i $todir
                    
                    justfilename=${i##*/}
                    echo "# Skipping $justfilename as will load within package" >> "$dirname"'\'"$dirname.txt"  
                    
                    #we want to run the load file loop
                    loadloop=1
                ;;
                *.unload)
                    #Will load from package so just need to add into package
                    #create folder
                    createfolder $dirname $unloaddir 
                    
                    #copy file
                    cp $i $todir
                    
                    justfilename=${i##*/}
                    echo "# Skipping $justfilename as will load within package" >> "$dirname"'\'"$dirname.txt"  
                    
                    #we want to run the load file loop
                    loadloop=1
                
                ;;
                *) filetyp="Not Moca"
             esac                                     
            
        else         
            echo "$i does not exist, skipping ..."
  fi
done

if [ $loadloop = 1 ]
    then
    
    #Second loop for loading
    echo "" >> "$dirname"'\'"$dirname.txt"   
    echo "##LOADING FILES" >> "$dirname"'\'"$dirname.txt"   

    for i in "${filearr[@]}"
    do
        #echo "Adding $i"
      if [ -f $i ]
            then            
                case "$i" in               
                    
                    *.ctl)
                        ##we assume the control file and csv are named the same 
                        file=${i##*/}
                        filepart=${file%.*}  
                        filepartcsv="$filepart.csv"
                        filepartctl="$filepart.csv"
                        justfilename=${i##*/}
                    
                        echo "LOADDATA $tocsvdir$file $filepartcsv" >> "$dirname"'\'"$dirname.txt"
                    
                    ;;
                    *.msql)
                        #get individual filename
                        file=${i##*/}
                    
                        #run it from package
                        echo "RUNSQL $scriptdir$file" >> "$dirname"'\'"$dirname.txt"
                       
                    ;;
                    *.sql)
                        #get individual filename
                        file=${i##*/}
                    
                        #run it from package
                        echo "RUNSQL $scriptdir$file" >> "$dirname"'\'"$dirname.txt"
                                 
                     ;;
                    *.seq)
                        #get individual filename
                        file=${i##*/}
                    
                        #run it from package
                        echo "RUNSQL $scriptdir$file" >> "$dirname"'\'"$dirname.txt"
                     
                    ;;
                    *.unload)
                       #get individual filename
                        file=${i##*/}
                    
                        #run it from package
                        echo "importsldata $unloaddir$file" >> "$dirname"'\'"$dirname.txt"
                                                                      
                    ;;
                    *) filetyp="Not Moca"
                 esac                                     
                
            else         
                echo "$i does not exist, skipping ..."
      fi
    done
fi

#If we need to mbuild lets add it
if [ $mbuild == 1 ]
    then  
    echo "" >> "$dirname"'\'"$dirname.txt" 
    echo "##MBUILD" >> "$dirname"'\'"$dirname.txt" 
    echo 'mbuild' >> "$dirname"'\'"$dirname.txt"
fi

echo "The amazing rollout script has completed ... enjoy"





